# pylint: disable=C0114
# Define the following variables
# first_name, last_name, age, eye_colour, hair_colour

FIRST_NAME = "Shannon"
LAST_NAME = "Tamplin"
EYE_COLOUR = "Blue"
HAIR_COLOUR = "Grey"
AGE = 26

# Print them back to the user as conversation
# Example: 'Hi there Jack! Your age is 36, you have green eye and black hair.


#Extra Question 2 - Year of birth calculation? and respond back.
# print example: 'You told the machine you are 28 hence you must have been born 1991.. give or take'


# Extra Question 3 - Cast your input so age is stored as numerical type
