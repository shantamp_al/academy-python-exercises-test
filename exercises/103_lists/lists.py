# Lists - LET's use some lists!
# Define a list with amazing things inside!
    # For example, best places to eat or things you would do if you won the lottery? 
    # It must have 5 items
    # Complete the sentence:
    # Lists are organized using: _______????? 


## Define a list with 5 amazing things inside!

my_amazing_list = [1, 2, 3, 4, 5]

# Print the lists and check the data type of the list?


# Print the list's first object

# Print the list's second object


# Check the type of the list's first and second object?
# is it the same as the list it self?


# Print the list's last object


# Re-define the second item on the list


# Re-define another item on the list and print all the list


# Add an item to the list then print the list


# Remove an item from the list then print the list



# Add two items to list then print the list



