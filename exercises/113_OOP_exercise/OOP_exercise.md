# OOP WICKES management system :hammer_and_wrench::toolbox:

This exercise if for your to practice OOP.

### Learning outcomes

- OOP 
- Git & bitbucket

Extra:

- Exception handling
- Testing 


### Task

We're going to build a system to creat a command line system to manage inventory and sales. 

Please see the user stories and Abstraction of what is involved in a Which store. 


### Abstraction and Outline of what need

So what's in the which store? What is involved in the world? Let's think of it so we can asbract complexity and creat what we need in classes and behaviour. 

#### What Exists, what are the characteristics? and what are the behaviours? 

- Customers
- Employees
- Procuts 
- Sales 

What are the charecteristic and behaviours? 

**Customers**

What are the characteristics?

- Name
- Email
- Phone number
- Tax number

What are the behaviours?

- You can creat one
- You delete one

**Employees**

What are the characteristics?

- Name
- Email
- Phone number
- Tax number
- Emplyee number

What are the behaviours?

- You can creat one
- You delete one

**Products**

What are the characteristics?

- Name
- Unic_ref
- Price
- Number_unit


What are the behaviours?

- You can creat it
- You can increate the number of units
- You can decrease the number of units

**Sales**

What are the characteristics?

- ID
- Who is the customer
- What is the item sold
- Number_units


What are the behaviours?

- You can creat a sale with the necessary parameter 
- Calculate price

### User stories

Imagine you are a sales person at Which - what do you need to do? 

#### User story 1

As a Wickes employee, I want to be able to list all existing products and their prices and avilability. So that I can show the client what he can by for how much. 

#### User story 2

As a Wickes employee, I want to be able to add a new product with the price and number of units available. So that it can be part of the inventory.

#### User story 3

As a Wickes employee, I want to be able to sell units of a product, so that we can make money. (**hint for acceptance criterea** what needs to happen to the stock?)

#### User story 4

As a Wickes employee, I want to be able to add multiple units of a product, so that we can add units increase our stock. (**hint for acceptance criterea** what needs to happen to the stock?)

#### User story 5

As a Wickes employee, I want to be able to register a user with their necessary details.

#### User story 5

As a Wickes employee, I want to be able to register a a sale with the necessary details.





